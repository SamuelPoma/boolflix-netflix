$(document).ready(function(){
// Milestone 1: (giovedì mattina 09.30)
// Creare un layout base con una searchbar (una input e un button) in cui possiamo scrivere completamente o parzialmente il nome di un film. Possiamo, cliccando il  bottone, cercare sull’API tutti i film che contengono ciò che ha scritto l’utente.
// Vogliamo dopo la risposta dell’API visualizzare a schermo i seguenti valori per ogni film trovato:
// Titolo
// Titolo Originale
// Lingua
// Voto
// Creo un evento click sul button che invia una chiamata API per ricercare il film inserito nella user-input
  var genres = [];
  genresApi();
  console.log()
  $("#btn-film").click(function(){
    // CALLBACK function, l'header presentation scompare dopo due secondi e viene
    // chiamata una funzione che esegue un insieme di istruzioni
    $(".header-presentation").fadeOut(2000, function(){
      searchApiMovieSeries();

    });
  });

// Creo una funzione che fa partire una chiamata AJAX dentro un'altra
// che cerca nel database l'input film inserito dall'utente
// e trova tutti i film contenenti le parole chiave digitate
  // devo generalizzare il processo di stampa tra film e serie di tv perché
  // sono molto simili e si può evitare di riscrivere codice simile
  function searchApiMovieSeries() {
    var inputFilm = $("#input-film").val();
    $.ajax({

        url:"https://api.themoviedb.org/3/search/movie",
        method:"GET",
        data:{
          api_key:'2b204907cf6bd21a064d92e7cec8e1ff',
          query: inputFilm
        },
        success: function (data) {
                  $("#film-container").html("");
                  // dentro la chiamata AJAX in success creo un ciclo for che mi permette di manipolare l'array contenente
                  // i film aventi le parole inserite dall'utente
                  var arrayFilms = data.results;
                  for (var i = 0; i < arrayFilms.length; i ++) {
                    var nameFilm = arrayFilms[i].title;
                    var originalTitle = arrayFilms[i].original_title;
                    var voteFilm = Math.round(arrayFilms[i].vote_average / 2);
                    languageFilm = arrayFilms[i].original_language;
                    var posterImg =  arrayFilms[i].poster_path;
                    var overview = arrayFilms[i].overview;
                    // console.log(posterImg);
                    // ricerca genere

                    var genre = arrayFilms[i].genre_ids;
                    for (var z = 0; z < genre.length; z++) {
                      var currentGenreId = genre[z];
                      // console.log(currentGenreId);
                    }
                    // inserisco un attribute alla classe .stars avente il valore di voteFilm
                    // mi servirà per costruire la mia funzione convertIntoStars
                    $("#film-container").append('<div class= img-container>'+ '<img class = poster-img  src =' + 'https://image.tmdb.org/t/p/w500' + posterImg + '>'
                    + '<div class = title-film>' + '<div class = title>' +  nameFilm + '</div>' + '<div class = original-title>' + "Original Title: " + originalTitle + '</div>' + '<div class = container-lang-stars>' + '<span class = "flag" language-film = "' + languageFilm + '">' + '</span>' + '<span class = "stars" number-vote = "' + voteFilm + '">' +  '</span>' + '</div>' + '<div class = overwiew>' + overview + '</div>' + '</div>' + '</div>');

                    $("#input-film").val(" ");
                      // BISOGNA RISOLVERE L'ERRORE

                    }

            $.ajax({
                url:"https://api.themoviedb.org/3/search/tv",
                method:"GET",
                data:{
                  api_key:'2b204907cf6bd21a064d92e7cec8e1ff',
                  query: inputFilm
                },
                success: function (data) {
                  $("#series-container").html("");
                  // dentro la chiamata AJAX in success creo un ciclo for che mi permette di manipolare l'array contenente
                  // i film aventi le parole inserite dall'utente
                  var arrayTvSeries = data.results;
                  for (var i = 0; i < arrayTvSeries.length; i ++) {
                    var nameSeries = arrayTvSeries[i].name;
                    var originalnameSeries = arrayTvSeries[i].original_name;
                    var voteFilm = Math.round(arrayTvSeries[i].vote_average / 2);
                    languageFilm = arrayTvSeries[i].original_language
                    var posterImg =  arrayTvSeries[i].poster_path

                    // inserisco un attribute alla classe .stars avente il valore di voteFilm
                    // mi servirà per costruire la mia funzione convertIntoStars
                    $("#series-container").append('<div class= img-container>'+ '<img class = poster-img src =' + 'https://image.tmdb.org/t/p/w500' + posterImg + '>'
                    +'<div class = "title-film">' + '<div class = title>' +  nameSeries + '</div>' + '<div class = original-title>' + "Original Title: " + originalnameSeries + '</div>' + '<div class = container-lang-stars>' + '<span class = "flag" language-film-sr = "' + languageFilm + '">' + '</span>' + '<span class = "stars" number-vote-sr = "' + voteFilm + '">' +  '</span>' + '</div>' + '<div class = overwiew>' + overview + '</div>' + '</div>' + '</div>' );
                    $("#input-film").val(" ");
                  }
                  convertVoteIntoStars();
                  languageStringIntoFlag();
                },
            });
        },
      });
  // creo una funzione che converte i valori numerici dei voti in stellina
  // controllo che per ogni elemento jquery .stars se this elemento ha
  // l'attr number-vote uguale ad un determinato numero e converto questo nel
  // suo valore in stelline.
  function convertVoteIntoStars(){
    $(".stars").each(function(){
        if ($(this).attr('number-vote') == 0)  {
          $(this).append('<i class="far fa-star">' + '</i>' + '<i class="far fa-star">' + '</i>' +
        '<i class="far fa-star">' + '</i>'+'<i class="far fa-star">' + '</i>'+'<i class="far fa-star">' + '</i>');
        }
        if ($(this).attr('number-vote') == 1) {
          $(this).append('<i class="fas fa-star">' + '</i>' + '<i class="far fa-star">' + '</i>' +
        '<i class="far fa-star">' + '</i>'+'<i class="far fa-star">' + '</i>'+'<i class="far fa-star">' + '</i>');
        }
        if ($(this).attr('number-vote') == 2) {
          $(this).append('<i class="fas fa-star">' + '</i>' + '<i class="fas fa-star">' + '</i>' +
        '<i class="far fa-star">' + '</i>'+'<i class="far fa-star">' + '</i>'+'<i class="far fa-star">' + '</i>');
        }
        if ($(this).attr('number-vote') == 3) {
          $(this).append('<i class="fas fa-star">' + '</i>' + '<i class="fas fa-star">' + '</i>' +
        '<i class="fas fa-star">' + '</i>'+'<i class="far fa-star">' + '</i>'+'<i class="far fa-star">' + '</i>');
        }
        if ($(this).attr('number-vote') == 4) {
          $(this).append('<i class="fas fa-star">' + '</i>' + '<i class="fas fa-star">' + '</i>' +
        '<i class="fas fa-star">' + '</i>'+'<i class="fas fa-star">' + '</i>'+'<i class="far fa-star">' + '</i>');
        }
        if ($(this).attr('number-vote') == 5) {
          $(this).append('<i class="fas fa-star">' + '</i>' + '<i class="fas fa-star">' + '</i>' +
        '<i class="fas fa-star">' + '</i>'+'<i class="fas fa-star">' + '</i>'+'<i class="fas fa-star">' + '</i>');
        }
    });
  }
  // creo una funzione che converte la lingua del film con l'icona flag corrispondente
  // utilizzando lo stesso procedimento per le stelline, ovvero inserendo un data attribute
  // che mi permette di effettuare un confronto diretto con il value dell'API
  function languageStringIntoFlag() {
    // gestisco il caso in cui la flag non viene trovata
    $(".flag").each(function(){
        if ($(this).attr('language-film') == "en") {
          $(this).append('<img class = "img-flag" src = "flag-img/gb.svg">');
        }
        if ($(this).attr('language-film') == "it") {
          $(this).append('<img class = "img-flag" src = "flag-img/it.svg">');
        }
        if ($(this).attr('language-film') == "fr") {
          $(this).append('<img class = "img-flag" src = "flag-img/fr.svg">');
        }
        if ($(this).attr('language-film') == "de") {
          $(this).append('<img class = "img-flag" src = "flag-img/de.svg">');
        }
        if ($(this).attr('language-film') == "fi") {
          $(this).append('<img class = "img-flag" src = "flag-img/fi.svg">');
        }
        // Caso limite in cui non trovo la flag
        if (!($(this).attr('language-film') == "en") &&
            !($(this).attr('language-film') == "it") &&
            !($(this).attr('language-film') == "fr") &&
            !($(this).attr('language-film') == "de") &&
            !($(this).attr('language-film') == "fi")) {
              $(this).append('<i class="fas fa-flag">&nbspFlag Not Found</i>');
      }
    });
   }
  }

  function genresApi() {
  $.ajax({

      url:"https://api.themoviedb.org/3/genre/movie/list",
      method:"GET",
      data:{
        api_key:'2b204907cf6bd21a064d92e7cec8e1ff',
      },
      success: function (data) {
        for (var i = 0 ; i < data.genres.length; i++ ) {
          genres.push(data.genres[i]);
        }
        console.log(genres);
      },
  });
}

// Al click sulla poster image appare il testo contenente le info del film
    $(document).on("click",".img-container",function(){
      var currentCard= $(this).find(".title-film ");
        currentCard.addClass("black-bg");
        currentCard.fadeIn(1000);
    });
    // On mouseleave scompare il testo contenente le info del film
    $(document).on("mouseleave",".img-container",function(){
      var currentCard= $(this).find(".title-film ");
        currentCard.fadeOut(1500);
    });


});
